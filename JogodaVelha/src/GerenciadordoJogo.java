import java.util.Scanner;

public class GerenciadordoJogo {

	
	private boolean fimDoJogo = false;
	static int[][] matrizGalo = new int[3][3];

	private int vezDoJogador = 1;

	private Scanner teclado = new Scanner(System.in);

	void iniciar() {
		int numVezesJogadas=0;
		System.out.println("");
		System.out.println("Seja bem vindo ao jogo do galo ");
		System.out.println("");

		while (!fimDoJogo) {

			printBoard();
			fimDoJogo=realizaJogada(numVezesJogadas);
			numVezesJogadas++;
		}

	}

	///////////////////////////////////////////////////////////////////////
	
	///Method to print the board	
	public static void printBoard() {
		System.out.println("   1   2  3");
		System.out.println(" -------------");

		for (int i = 0; i < 3; i++) {
			System.out.print((i + 1) + "| ");
			for (int j = 0; j < 3; j++) {
				System.out.print(matrizGalo[i][j] + " | ");
			}
			System.out.println();
			System.out.println(" -------------");
		}
	}

	///////////////////////////////////////////////////////////////////////////
	
	///Method to input	
	boolean realizaJogada(int numVezesJogadas) {
		boolean posicaoPermitida = true;
		int linha, coluna;
	
	
		do {
			System.out.println("Jogador " + vezDoJogador);
			System.out.println("Digite a linha entre 1 e 3");
			linha = teclado.nextInt();
			System.out.println("Digite a coluna entre 1 e 3");
			coluna = teclado.nextInt();

			if (linha <= 3 && linha >= 1 && coluna <= 3 && coluna >= 1 && matrizGalo[linha - 1][coluna - 1] > 0) {
				System.out.println("Jogada inv�lida");
				posicaoPermitida = false;
			} else {
				matrizGalo[linha - 1][coluna - 1] = vezDoJogador;
				posicaoPermitida = true;
			}

		} while (!posicaoPermitida);
		
	
		
		int ganhoujogador= avaliaEstadoDoJogo(vezDoJogador);
		if (ganhoujogador==1 || ganhoujogador==2) {
			apresentarResultado(vezDoJogador);
			return true;
		}
		
		
		System.out.println("Jogada "+ numVezesJogadas);
		if (numVezesJogadas==8){
			apresentarResultado(3);
			return true;
		}
			


		vezDoJogador = ((vezDoJogador) % 2) + 1;

		

		return false;
		

	}
	
	///Method to check the board

	int avaliaEstadoDoJogo(int teste) {
		

		int JogadorGanhou=-1;
		if (matrizGalo[0][0]==teste && matrizGalo[0][1] == teste && matrizGalo[0][2] == teste)
			JogadorGanhou = teste;
		if (matrizGalo[1][0]==teste && matrizGalo[1][1] == teste && matrizGalo[1][2] == teste)
			JogadorGanhou = teste;
		if (matrizGalo[2][0]==teste && matrizGalo[2][1] == teste && matrizGalo[2][2] == teste)
			JogadorGanhou = teste;
		if (matrizGalo[0][0]==teste && matrizGalo[1][0] == teste && matrizGalo[2][0] == teste)
			JogadorGanhou = teste;
		if (matrizGalo[1][0]==teste && matrizGalo[1][1] == teste && matrizGalo[2][1] == teste)
			JogadorGanhou = teste;
		if (matrizGalo[2][0]==teste && matrizGalo[2][1] == teste && matrizGalo[2][2] == teste)
			JogadorGanhou = teste;
		if (matrizGalo[0][0]==teste && matrizGalo[1][1] == teste && matrizGalo[2][2] == teste)
			JogadorGanhou = teste;
		if (matrizGalo[2][0]==teste && matrizGalo[1][1] == teste && matrizGalo[0][2] == teste)
			JogadorGanhou = teste;

		 
		return (JogadorGanhou);
	
	}

	///Method to check winner, loser or draw	
	void apresentarResultado(int jogadorvenceu) {

		if (jogadorvenceu==1) {
			System.out.println("O jogador 1 venceu!!!");
		} else if (jogadorvenceu==2) {
			System.out.println("O jogador 2 venceu!!!");
		} else if (jogadorvenceu==3){
			System.out.println("Empate t�cnico!");

		}
	}
}